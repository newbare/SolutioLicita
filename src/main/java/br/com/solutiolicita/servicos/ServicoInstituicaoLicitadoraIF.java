package br.com.solutiolicita.servicos;

import br.com.solutiolicita.modelos.InstituicaoLicitadora;

/**
 *
 * @author ricardocaldeira
 */
public interface ServicoInstituicaoLicitadoraIF extends ServicoIF<InstituicaoLicitadora>{
    
}
